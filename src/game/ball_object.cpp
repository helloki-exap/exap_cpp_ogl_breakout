
#include "ball_object.h"

hll::BallObject::BallObject()
    : hll::GameObject(), radius(12.5f), is_stuck(true) { }

hll::BallObject::BallObject(glm::vec2 pos, float rad, glm::vec2 vel, unsigned int tex)
    : GameObject(pos, glm::vec2(rad * 2.0f, rad * 2.0f), tex, glm::vec3(1.0f), vel), radius(rad), is_stuck(true) { }

glm::vec2 hll::BallObject::move(float dt, unsigned int window_width) {
    if (this->is_stuck) { return this->pos; }

    this->pos += this->vel * dt;

    // check if outside bounds
    if (this->pos.x <= 0.0f) {
        this->vel.x = -this->vel.x;
        this->pos.x = 0.0f;
    } else if (this->pos.x + this->size.x >= window_width) {
        this->vel.x = -this->vel.x;
        this->pos.x = window_width - this->size.x;
    }

    if (this->pos.y <= 0.0f) {
        this->vel.y = -this->vel.y;
        this->pos.y = 0.0f;
    }

    return this->pos;
}

void hll::BallObject::reset(glm::vec2 pos, glm::vec2 vel) {
    this->pos = pos;
    this->vel = vel;
    this->is_stuck = true;
}