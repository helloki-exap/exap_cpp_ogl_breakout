
#ifndef HELLENGINE_COLLIDER_H
#define HELLENGINE_COLLIDER_H

#include <glm/glm.hpp>

namespace hll::collider {


enum class Direction {
    UP = 0,
    DOWN = 1,
    LEFT = 2,
    RIGHT = 3,
};

typedef std::tuple<bool, Direction, glm::vec2> CollisionInfo;

bool check_collision_2d_aabb_aabb(glm::vec2 obj_1_tl, glm::vec2 obj_1_br, glm::vec2 obj_2_tl, glm::vec2 obj_2_br);
CollisionInfo check_collision_2d_circle_aabb(glm::vec2 cir_center, float cir_radius, glm::vec2 box_tl, glm::vec2 box_size);
Direction calc_vec_direction(glm::vec2 target);

}


#endif //HELLENGINE_COLLIDER_H
