#ifndef HELLENGINE_SPRITE_RENDERER_H
#define HELLENGINE_SPRITE_RENDERER_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "sprite_renderer.h"
#include "texture_2d.h"
#include "shader.h"

namespace hll::sprite_renderer {

    struct RenderInfo {
        const unsigned int shader;
        const unsigned int vao;
    };

    hll::sprite_renderer::RenderInfo create_data(unsigned int shader);
    void delete_data(unsigned int vao);
    void draw_sprite(const RenderInfo &info, unsigned int tex, glm::vec2 pos, glm::vec2 size, float rot_deg, glm::vec3 color);

}


#endif //HELLENGINE_SPRITE_RENDERER_H